<?php
/*
  Plugin Name: Boarding Registration
  Description: This is the boarding registration process for validating users with the council for cat and dog home boarding registrations. Additional checks will be made for CRB validity also.
  Version: 1.0
  Author: Dom King
  Author URI: https://gmcyberfoundry.ac.uk/
*/

if ( ! class_exists( 'WP_List_Table' ) )
{
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Boardings_List extends WP_List_Table
{
    /** Class constructor */
    public function __construct()
    {
        parent::__construct( [
            'singular' => __( 'Boarding', 'hmp' ), //singular name of the listed records
            'plural'   => __( 'Boardings', 'hmp' ), //plural name of the listed records
            'ajax'     => false //does this table support ajax?
        ] );
    }

    /**
     * Retrieve boarding data from the database
     *
     * @param int $per_page
     * @param int $page_number
     *
     * @return mixed
     */
    public static function get_boardings( $per_page = 10, $page_number = 1 )
    {

        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}home_boarding";

        if ( ! empty( $_REQUEST['orderby'] ) ) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }

        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


        $result = $wpdb->get_results( $sql, 'ARRAY_A' );

        return $result;
    }

    /**
     * Returns the count of records in the database.
     *
     * @return null|string
     */
    public static function record_count()
    {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}home_boarding";

        return $wpdb->get_var( $sql );
    }


    /** Text displayed when no user data is available */
    public function no_items()
    {
        _e( 'No users registered.', 'hmp' );
    }

    /**
     * Render a column when no column specific method exist.
     *
     * @param array $item
     * @param string $column_name
     *
     * @return mixed
     */
    public function column_default( $item, $column_name )
    {
        switch ( $column_name ) {
            case 'user_id':
            case 'first_name':
            case 'last_name':
            case 'telephone':
            case 'boarding':
            case 'council_number':
            case 'council_email':
            case 'council_telephone':
            case 'council_file':
            case 'insurance_file':
            case 'date_registered':
            case 'verified':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ); //Show the whole array for troubleshooting purposes
        }
    }

    /**
     * Render the bulk edit checkbox
     *
     * @param array $item
     *
     * @return string
     */
    function column_cb( $item )
    {
        return sprintf(
            '<input type="checkbox" name="Boarding[]" value="%s" />', $item['user_id']
        );
    }

    /**
     *  Associative array of columns
     *
     * @return array
     */
    function get_columns()
    {
        $columns = [
            'cb'      => '<input type="checkbox" />',
            'user_id'    => __( 'ID', ';', 'hmp' ),
            'first_name'    => __( 'First Name', ';', 'hmp' ),
            'last_name' => __( 'Last Name', 'hmp' ),
            'telephone'    => __( 'Telephone', 'hmp' ),
            'boarding'    => __( 'Boarding Type', 'hmp' ),
            'council_number'    => __( 'Certificate Number', 'hmp' ),
            'council_email'    => __( 'Council Email', 'hmp' ),
            'council_telephone'    => __( 'Council Telephone', 'hmp' ),
            'council_file'    => __( 'Licence', 'hmp' ),
            'insurance_file'    => __( 'Insurance', 'hmp'),
            'date_registered'    => __( 'Date Registered', 'hmp' ),
            'verified'    => __( 'Verified', 'hmp' ),
        ];

        return $columns;
    }

    /**
     * Columns to make sortable.
     *
     * @return array
     */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'user_id' => array( 'user_id', true ),
            'verified' => array( 'verified', true ),
            'boarding' => array( 'boarding', true ),
            'date_registered' => array( 'date_registered', true ),
        );

        return $sortable_columns;
    }

    /**
     * Returns an associative array containing the bulk action
     *
     * @return array
     */
    public function get_bulk_actions()
    {
        $actions = [
            'bulk-delete' => 'Delete',
            'bulk-verify' => 'Verify',
            'bulk-email'  => 'Resend Email'
        ];

        return $actions;
    }


    /**
     * Handles data query and filter, sorting, and pagination.
     */
    public function prepare_items()
    {
        $this->_column_headers = $this->get_column_info();

        /** Process bulk action */
        $this->process_bulk_action();

        $per_page     = $this->get_items_per_page( 'users_per_page', 10 );
        $current_page = $this->get_pagenum();
        $total_items  = self::record_count();

        $this->set_pagination_args( [
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page'    => $per_page //WE have to determine how many items to show on a page
        ] );

        $this->items = self::get_boardings( $per_page, $current_page );
    }

    // Process the bulk actions within here - delete, verify and send emails
    public function process_bulk_action()
    {
        $action = $this->current_action();

        if( empty( $action ) )
            return;

	    // If the delete bulk action is triggered
        if ( 'bulk-delete' === $this->current_action() )
        {
            $delete_ids = esc_sql( $_POST['Boarding'] );

            // loop over the array of record IDs and delete them
            foreach ( $delete_ids as $id )
            {
                self::delete_user( $id );
            }

	        // The redirect won't work as it will cause header issues so I used a die to display message
	        wp_die('User has been successfully deleted');
            //wp_redirect( esc_url_raw(add_query_arg()) );
	        //exit;
        }

        // If the verify bulk action is triggered
        if ( 'bulk-verify' === $this->current_action() )
        {
            $verify_ids = esc_sql( $_POST['Boarding'] );

            // loop over the array of record IDs and verify them
            foreach ( $verify_ids as $id )
            {
                self::verify_user( $id );
            }

	        // The redirect won't work as it will cause header issues so I used a die to display message
	        wp_die('User has been successfully verified');
	        //wp_redirect( esc_url_raw(add_query_arg()) );
	        //exit;
        }

        // If the email bulk action is triggered
        if ( 'bulk-email' === $this->current_action() )
        {
            $email_ids = esc_sql( $_POST['Boarding'] );

            // loop over the array of record IDs and verify them
            foreach ( $email_ids as $id )
            {
                self::process_council_email( $id );
            }

	        // The redirect won't work as it will cause header issues so I used a die to display message
	        wp_die('Email to the council has been sent successfully');
	        //wp_redirect( esc_url_raw(add_query_arg()) );
	        //exit;
        }
    }

    /**
     * Delete a boarding user record.
     *
     * @param int $id user ID
     */
    public static function delete_user( $id )
    {
        global $wpdb;

        // Delete the user from the home boarding table
        $wpdb->delete(
            "{$wpdb->prefix}home_boarding",
            [ 'user_id' => $id ],
            [ '%d' ]
        );

	    // Delete the user from the user table also
	    $wpdb->delete(
            "{$wpdb->prefix}users",
            [ 'ID' => $id ],
            [ '%d' ]
        );
    }

    /**
     * Verify a boarding user record.
     *
     * @param int $id user ID
     */
    public static function verify_user( $id )
    {
        global $wpdb;
        $home_boarding_table = $wpdb->prefix."home_boarding";

        $wpdb->update( $home_boarding_table, array( 'verified' => "Yes" ), array( 'user_id' => $id ));

        $u = new WP_User( $id );
        // Remove role
        $u->remove_role( 'boarder' );
        // Add role
        $u->add_role( 'verified_boarder' );
    }

	/**
	 * Send a council email for each ID.
	 *
	 * @param int $id user ID
	 */
	public static function process_council_email( $id )
	{
		global $wpdb;
		$home_boarding_table = $wpdb->prefix."home_boarding";

		$result = $wpdb->get_results( "
            SELECT * 
            FROM  $home_boarding_table
                WHERE user_id = $id
        " );

		foreach($result as $r){
			self::resend_council_email($r->first_name, $r->last_name, $r->council_number, $r->council_email);
		}
	}

	// Send the council an email
    public static function resend_council_email( $first_name, $last_name, $council_number, $council_email )
	{
		$to = $council_email;
		$from = 'support@housemypet.com';
		$fromName = 'House my Pet';
		$subject = "Home Boarding Licence: $council_number.";

		// Get HTML contents from file
		$plugin_url = plugin_dir_url( dirname( __FILE__ ) );

		$htmlContent = file_get_contents(dirname( __DIR__ ) . '/mail/council-email.php');
		$htmlContent = str_replace("[fname]","$first_name", $htmlContent);
		$htmlContent = str_replace("[lname]","$last_name", $htmlContent);
		$htmlContent = str_replace("[cnumber]","$council_number", $htmlContent);
		$htmlContent = str_replace("[pluginurl]","$plugin_url", $htmlContent);

		// Set content-type header for sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// Additional headers
		$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n";
		$headers .= 'Cc: sheena@housemypet.com' . "\r\n";

		// Send email
		mail($to, $subject, $htmlContent, $headers);

	}
}


class HMP_Plugin
{
    // class instance
    static $instance;

    // customer WP_List_Table object
    public $users_obj;

    // class constructor
    public function __construct()
    {
        add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
        add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
    }


    public static function set_screen( $status, $option, $value )
    {
        return $value;
    }

    public function plugin_menu()
    {
        $hook = add_menu_page(
            'Home Boarding Users',
            'Home Boarding',
            'manage_options',
            'wp_list_table_class',
            [ $this, 'plugin_settings_page' ],
            'dashicons-admin-home'
        );

        add_action( "load-$hook", [ $this, 'screen_option' ] );
    }


    /**
     * Plugin settings page
     */
    public function plugin_settings_page() {
        ?>
        <div class="wrap">
            <h2>Home Boarding Users</h2>
            <p>Welcome to your user list of home boarders. Below you will find a dropdown which will allow you to bulk verify, delete and send emails for selected home boarding users. Additionally, if you wish to view them by verified status or type of boarding you can click on the tab to list them accordingly.
            </p>

            <div id="post-body" class="metabox-holder columns-2">
                <div id="post-body-content">
                    <div class="meta-box-sortables ui-sortable">
                        <form method="post">
                            <?php
                            $this->users_obj->prepare_items();
                            $this->users_obj->display(); ?>
                        </form>
                    </div>
                </div>
            </div>
            <br class="clear">
        </div>
        <?php
    }

    /**
     * Screen options
     */
    public function screen_option()
    {
        $option = 'per_page';
        $args   = [
            'label'   => 'Home Boarding',
            'default' => 10,
            'option'  => 'users_per_page'
        ];

        add_screen_option( $option, $args );

        $this->users_obj = new Boardings_List();
    }


    /** Singleton instance */
    public static function get_instance()
    {
        if ( ! isset( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}

add_action( 'plugins_loaded', function ()
{
    HMP_Plugin::get_instance();
} );