<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'housemypet_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';m=!Up]swE<WCa-?(W{X{L1^,C_M$usp*YLdO3?h]:p#;2jy>!MkCeY[?i`*%C_M' );
define( 'SECURE_AUTH_KEY',  'bFm7_@kGrJ+f(on/^q,20RI1*<Z0#r,4$m.RvNm5N-L hQ11YL6ocyfu:(n9chO{' );
define( 'LOGGED_IN_KEY',    'l#1Ib~lG%Q@OVd_){2$+uE8wYwXn:2f89)ry{Z}rmI_Ds9H+V^/Mk|l nzDc3>Eg' );
define( 'NONCE_KEY',        'tJeL94U8f55chiId=ouMq[;sH`H~pXdBo?}[4P,6F4%x}EJgrk-Z>yY^(V#  g]q' );
define( 'AUTH_SALT',        '5Da)<0 R8]!0_2Mi)pwt@O>T6)#s-.gBgmX*#Ii>jR}Q*m.>lF:mv/O_3>pnp`|j' );
define( 'SECURE_AUTH_SALT', '@T)w?(/Y7p*q~p84@J2Qie-(F:(+40 `76q<HW9mRtYcW{L|o)IJ% <[~&a!DaNC' );
define( 'LOGGED_IN_SALT',   '`Ni/t6g3(K P*EP|M@ *$7jl-R0dka_X/PV.(U[L}mc;WC#,C~P7.v+bsC%4NQT1' );
define( 'NONCE_SALT',       '*!@~I7;@SPQB)a+T(<IcVJ!D7zY >ePzhaXcP,AUB|oQQ9^nKA{8xL-#)@B/A(ak' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* TODO - This is for development purposes for local http testing - please delete for production */
define('WP_HOME','http://domking.local/');
define('WP_SITEURL','http://domking.local/');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
