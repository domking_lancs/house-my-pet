<?php
/*
  Plugin Name: Boarding Registration
  Description: This is the boarding registration process for validating users with the council for cat and dog home boarding registrations. Additional checks will be made for CRB validity also. 
  Version: 1.0
  Author: Dom King
  Author URI: https://gmcyberfoundry.ac.uk/
 */

// Firstly include the directory for the custom admin tables
include(dirname(__FILE__) . "/admin/boarding-admin-area.php");

/**
 * Register jquery and style on initialization
 */
add_action('init', 'boarding_assets');
function boarding_assets()
{
    wp_register_style( 'boarding_css', plugins_url('/assets/css/home-boarding.css', __FILE__), false, '1.0.0', 'all');
}

/**
 * Use the registered jquery and style above
 */
add_action('wp_enqueue_scripts', 'enqueue_style');
function enqueue_style()
{
    wp_enqueue_style('boarding_css');
}

/**
 * @return bool
 * When the plugin is installed, create the boarding table sutomatically within the database
 */
function create_home_boarding_table()
{
	global $wpdb;
	$tableName = $wpdb->prefix."home_boarding";
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $tableName (
	  user_id int(11) NOT NULL,
	  first_name varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  last_name varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  town varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  telephone varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  boarding varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  council_number varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  council_email varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  council_telephone varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
	  council_file blob NOT NULL,
	  insurance_file blob NOT NULL,
	  gdpr varchar(11) NOT NULL,
	  date_registered datetime DEFAULT NULL,
	  verified varchar(11) NOT NULL DEFAULT 0,
	  PRIMARY KEY (user_id)
	) $charset_collate;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

	$success = empty($wpdb->last_error);
	return $success;
}

/**
 * Display the registration form template
 */
function display_registration_form()
{
	include "templates/boarding-registration-form.php";
}
 
/**
 * Add the shortcode which can be embedded to any page that the registration form needs to be displayed
 */
add_shortcode( 'boarding_registration_form', 'wp_custom_shortcode_registration' );
 
/**
 * @return false|string
 * Custom shortcode registration - saves data to the buffer then discards once submitted
 */
function wp_custom_shortcode_registration()
{
    ob_start();
	create_home_boarding_table();
	display_registration_form();
    return ob_get_clean();
}