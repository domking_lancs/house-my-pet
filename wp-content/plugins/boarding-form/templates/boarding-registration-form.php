<?php
/**
 * Created by PhpStorm.
 * User: DominicKing
 * Date: 15/12/20
 * Time: 13:36
 */

/**
 * Call the registration form function in order to display the form
 */
boarding_registration_form_function();

/**
 * @param $first_name
 * @param $last_name
 * @param $town
 * @param $email
 * @param $username
 * @param $password
 * @param $telephone
 * @param $boarding
 * @param $council_number
 * @param $council_email
 * @param $council_telephone
 * @param $council_file
 * @param $insurance_file
 * @param $gdpr
 * Create the registration form layout we will be using to store and capture home boarding data
 */
function boarding_registration_form( $first_name, $last_name, $town, $email, $username, $password, $telephone, $boarding, $council_number, $council_email, $council_telephone, $council_file, $insurance_file, $gdpr )
{
	global $customize_error_validation, $first_name, $last_name, $town, $email, $username, $password, $telephone, $boarding, $council_number, $council_email, $council_telephone;

	// Get the current page on the form
	$current_page  =  $_SERVER['REQUEST_URI'];

	// Check for $_POST value for the page_count, if on page 1 it will be null
	if ($_POST && $_POST['nextPage']) {
		$page_count   =   $_POST['nextPage'];
	} else {
		$page_count = null;
	}

	// Display the pages depending on page count:
	// Note: these forms could be further broken up into their own templates in order to be modular
	if ( $page_count == NULL ) {
		echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details ">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                <div class="icon-logo">
                    </div>
					    </div>
					    <h1 class="boarding-title">Home Boarding Registration</h1>
					    <div class="terms"><a href="#terms-popup">Terms &amp; Conditions</a></div>
					    
						<div id="terms-popup" class="popup-overlay">
							<div class="terms-popup">
								<h2>Terms &amp; Conditions</h2>
								<a class="popup-close" href="#">&times;</a>
								<div class="popup-content">
									House my Pet requires that you follow your local laws and regulations regarding licensing and other requirements. Requirements vary by location, so check with your local council to learn about the regulations where you live. If you are in the UK then you will need a license in order to home board cats and dogs. House my Pet will not be held responsible for any legal action taken against you should you fail to adhere to your local laws and regulations regarding licensing and other requirements.
								</div>
							</div>
						</div>
					</div>
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">Registration Details</div>
                        <div class="form-subtitle">Please provide us with your user registration details in order to create an account. Please read our terms &amp; conditions.</div>
                        <div class="form">
                            <form action="' . $current_page . '" method="post" enctype="multipart/form-data">
                                <p class="content-item">
                                    <label>first name
                                        <input type="text" name="fname" maxlength="25" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="content-item">
                                    <label>last name
                                        <input type="text" name="lname" maxlength="25" value="' . ( isset( $_POST['lname']) ? $last_name : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="content-item">
                                    <label>email address
                                        <input type="text" name="email" maxlength="40" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="' . ( isset( $_POST['email']) ? $email : null ) . '" required>
                                    </label>
                                </p>
                                
                              	<p class="content-item">
                                    <label>Username
                                        <input type="text" name="username" maxlength="25" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '" required>
                                    </label>
                                </p>

                                <p class="content-item">
                                    <label>password
                                        <input type="password" name="password" maxlength="25" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '" required>
                                    </label>
                                </p>
                				
                				<input type="hidden" value="1" name="nextPage" />
                                <button type="submit"  class="signup">Next</button>
                            </>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    ';
	}//end page 1
	if ( $page_count == 1 ) {

		$first_name     = $_POST['fname'];
		$last_name      = $_POST['lname'];
		$email          = $_POST['email'];
		$username       = $_POST['username'];
		$password       = $_POST['password'];

		echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details ">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                <div class="icon-logo">
        
                    </div>
                        </div>
                        <h1 class="boarding-title">Home Boarding Registration</h1>
					    <div class="terms"><a href="#terms-popup">Terms &amp; Conditions</a></div>
                        
						<div id="terms-popup" class="popup-overlay">
							<div class="terms-popup">
								<h2>Terms &amp; Conditions</h2>
								<a class="popup-close" href="#">&times;</a>
								<div class="popup-content">
									House my Pet requires that you follow your local laws and regulations regarding licensing and other requirements. Requirements vary by location, so check with your local council to learn about the regulations where you live. If you are in the UK then you will need a license in order to home board cats and dogs. House my Pet will not be held responsible for any legal action taken against you should you fail to adhere to your local laws and regulations regarding licensing and other requirements.
								</div>
							</div>
						</div>
                    </div>
                    
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">Additional Details</div>
                        <div class="form-subtitle">Please provide us with your user registration details in order to create an account. Please read our terms &amp; conditions.</div>
                        <div class="form">
                            <form action="' . $current_page . '" method="post" enctype="multipart/form-data">
                            
                                <input type="hidden" name="fname" id="fname" value="'. $first_name .'" />
                                <input type="hidden" name="lname" id="lname" value="'. $last_name .'" />
                                <input type="hidden" name="email" id="email" value="'. $email .'" />
                                <input type="hidden" name="username" id="username" value="'. $username .'" />
                                <input type="hidden" name="password" id="password" value="'. $password .'" />
                                
                                <p class="content-item">
                                    <label>town
                                        <input type="text" name="town" maxlength="25" value="' . ( isset( $_POST['town']) ? $town : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="content-item">
                                    <label>telephone
                                        <input type="text" name="telephone" maxlength="15" value="' . ( isset( $_POST['telephone']) ? $telephone : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="form-subtitle">Please select which home boarding you are registering for below:</p>
                                <p class="content-item">
                                    <label>Cat Boarding
                                        <input type="radio" id="catboarding" name="boarding" value="Cat" checked>
                                    </label>
                                </p>
                                
                                 <p class="content-item">
                                    <label>Dog Boarding
                                        <input type="radio" id="dogboarding" name="boarding" value="Dog">
                                    </label>
                                </p>
                                
                				<input type="hidden" value="2" name="nextPage" />
                				
                                <button type="submit" class="signup" onclick="window.history.go(-1); return false;">Go Back</button>
                                <button type="submit"  class="signup">Next</button>
                            </>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    ';
	} if ( $page_count == 2 ) {

	$first_name     = $_POST['fname'];
	$last_name      = $_POST['lname'];
	$email          = $_POST['email'];
	$username       = $_POST['username'];
	$password       = $_POST['password'];
	$town           = $_POST['town'];
	$telephone      = $_POST['telephone'];
	$boarding       = $_POST['boarding'];

	echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details ">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                <div class="icon-logo">
        
                    </div>
                        </div>
                        <h1 class="boarding-title">Home Boarding Registration</h1>
					    <div class="terms"><a href="#terms-popup">Terms &amp; Conditions</a></div>
                        
						<div id="terms-popup" class="popup-overlay">
							<div class="terms-popup">
								<h2>Terms &amp; Conditions</h2>
								<a class="popup-close" href="#">&times;</a>
								<div class="popup-content">
									House my Pet requires that you follow your local laws and regulations regarding licensing and other requirements. Requirements vary by location, so check with your local council to learn about the regulations where you live. If you are in the UK then you will need a license in order to home board cats and dogs. House my Pet will not be held responsible for any legal action taken against you should you fail to adhere to your local laws and regulations regarding licensing and other requirements.
								</div>
							</div>
						</div>
                    </div>
                    
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">Council Certification</div>
                        <div class="form-subtitle">Please provide us with your user registration details in order to create an account. Please read our terms &amp; conditions.</div>
                        <div class="form">
                            <form action="' . $current_page . '" method="post" enctype="multipart/form-data">
                            
                                <input type="hidden" name="fname" id="fname" value="'. $first_name .'" />
                                <input type="hidden" name="lname" id="lname" value="'. $last_name .'" />
                                <input type="hidden" name="email" id="email" value="'. $email .'" />
                                <input type="hidden" name="username" id="username" value="'. $username .'" />
                                <input type="hidden" name="password" id="password" value="'. $password .'" />
                                <input type="hidden" name="town" id="town" value="'. $town .'" />
                                <input type="hidden" name="telephone" id="telephone" value="'. $telephone .'" />
                                <input type="hidden" name="boarding" id="boarding" value="'. $boarding .'" />

                                <p class="content-item">
                                    <label>Certificate Number
                                        <input type="text" name="cnumber" maxlength="25" value="' . ( isset( $_POST['cnumber']) ? $council_number : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="content-item">
                                    <label>Council Email
                                        <input type="text" name="cemail" maxlength="40" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="' . ( isset( $_POST['cemail']) ? $council_email : null ) . '" required>
                                    </label>
                                </p>
                                
                                <p class="content-item">
                                    <label>Council Telephone
                                        <input type="text" name="ctelephone" maxlength="25" value="' . ( isset( $_POST['ctelephone']) ? $council_telephone : null ) . '" required>
                                    </label>
                                </p>
                                
                				<input type="hidden" value="3" name="nextPage" />
                				
                				<button type="submit" class="signup" onclick="window.history.go(-1); return false;">Go Back</button>
                                <button type="submit" class="signup">Next</button>
                            </>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
   ';
} if ( $page_count == 3 ) {

	$first_name         = $_POST['fname'];
	$last_name          = $_POST['lname'];
	$email              = $_POST['email'];
	$username           = $_POST['username'];
	$password           = $_POST['password'];
	$town               = $_POST['town'];
	$telephone          = $_POST['telephone'];
	$boarding           = $_POST['boarding'];
	$council_number     = $_POST['cnumber'];
	$council_email      = $_POST['cemail'];
	$council_telephone  = $_POST['ctelephone'];

	echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details ">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                <div class="icon-logo">
        
                    </div>
                        </div>
                        <h1 class="boarding-title">Home Boarding Registration</h1>
					    <div class="terms"><a href="#terms-popup">Terms &amp; Conditions</a></div>
                        
						<div id="terms-popup" class="popup-overlay">
							<div class="terms-popup">
								<h2>Terms &amp; Conditions</h2>
								<a class="popup-close" href="#">&times;</a>
								<div class="popup-content">
									House my Pet requires that you follow your local laws and regulations regarding licensing and other requirements. Requirements vary by location, so check with your local council to learn about the regulations where you live. If you are in the UK then you will need a license in order to home board cats and dogs. House my Pet will not be held responsible for any legal action taken against you should you fail to adhere to your local laws and regulations regarding licensing and other requirements.
								</div>
							</div>
						</div>
                    </div>
                    
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">Declaration and Files</div>
                        <div class="form-subtitle">Please provide us with your user registration details in order to create an account. Please read our terms &amp; conditions.</div>
                        <div class="form">
                            <form action="' . $current_page . '" method="post" enctype="multipart/form-data">
                            
                                <input type="hidden" name="fname" id="fname" value="'. $first_name .'" />
                                <input type="hidden" name="lname" id="lname" value="'. $last_name .'" />
                                <input type="hidden" name="email" id="email" value="'. $email .'" />
                                <input type="hidden" name="username" id="username" value="'. $username .'" />
                                <input type="hidden" name="password" id="password" value="'. $password .'" />
                                <input type="hidden" name="town" id="town" value="'. $town .'" />
                                <input type="hidden" name="telephone" id="telephone" value="'. $telephone .'" />
                                <input type="hidden" name="boarding" id="boarding" value="'. $boarding .'" />
                                <input type="hidden" name="cnumber" id="cnumber" value="'. $council_number .'" />
                                <input type="hidden" name="cemail" id="cemail" value="'. $council_email .'" />
                                <input type="hidden" name="ctelephone" id="ctelephone" value="'. $council_telephone .'" />
                                
                             	<p class="content-item">
	                                <label>Council Licence (PDF, JPG, PNG)
	                                    <input type="file" id="cfile" name="cfile" aria-required=”true” accept="application/msword, application/vnd.ms-excel, application/pdf, image/*" required multiple=”false” required>
	                                </label>
                                </p>
                                
                                <p class="content-item">
	                                <label>Insurance Certificate (PDF, JPG, PNG)
	                                    <input type="file" id="ifile" name="ifile" aria-required=”true” accept="application/msword, application/vnd.ms-excel, application/pdf, image/*" required multiple=”false” required>
	                                </label>
                                </p>

                                <p class="content-item">
                                    <label>I confirm I have read and accept the terms and conditions*
                                        <input type="checkbox" name="gdpr" value="' . ( isset( $_POST['gdpr']) ? 'No' : 'Yes' ) . '" required>
                                    </label>
                                </p>
                                
                				<input type="hidden" value="4" name="nextPage" />
                				
                				<button type="submit" class="signup" onclick="window.history.go(-1); return false;">Go Back</button>
                                <button type="submit" name="submit" class="signup">Submit</button>
                            </>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
   ';
}  if ( $page_count == 4 && 1 > count( $customize_error_validation->get_error_messages() ) ) {
	echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details-success">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                			<div class="icon-logo"></div>
                        </div>
                        <h1 class="boarding-title">Registration Successful</h1>
                    </div>
                    
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">Thankyou for Registering</div>
                        <div class="form-subtitle">You will receive an email within your inbox within the next 24 - 48 hours in regards to your application. We will send your details to the corresponding councils in order to gain validation.</div>
                        <div class="form">
                            <a href="https://www.housemypet.com/wp-login.php"><button type="submit" name="account" class="signup">Login to your Account</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
   ';
} elseif ( $page_count == 4 && 2 > count( $customize_error_validation->get_error_messages() )) {
	echo '
        <div class="boarding-content-wrapper">
        <div class="boarding-content">
            <div class="signup-wrapper shadow-box">
                <div class="company-details-unsuccess">
                    <div class="shadow"></div>
                    <div class="wrapper-left">
                        <div class="boarding-logo">
                			<div class="icon-logo"></div>
                        </div>
                        <h1 class="boarding-title">Registration Unsuccessful</h1>
                    </div>
                    
                </div>
                <div class="signup-form ">
                    <div class="wrapper-right">
                        <div class="form-title">There was an error with your registration</div>
                        <div class="form-subtitle">Please contact us directly on .... to discuss your registration issues.</div>
                        <div class="form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
   ';
}
}

/**
 * @param $username
 * @param $password
 * @param $email
 * @param $first_name
 * @param $last_name
 * Validates the registration form to check for empty fields and existing users
 * This currently focuses on the first page for validating, ideally when implementing this into the new system it will require
 * a better way to validate the form depending on how it is going to be used in production.
 */
function wp_reg_form_valid( $username, $password, $email, $first_name, $last_name )
{
	global $customize_error_validation;
	$customize_error_validation = new WP_Error;
	if ( empty( $username ) || empty( $password ) || empty( $email ) || empty( $first_name ) || empty( $last_name )) {
		$customize_error_validation->add('field', ' Please complete all of the form fields');
	}
    if ( username_exists( $username ) )
        $customize_error_validation->add('user_name', ' Username Already Exist');
	if ( is_wp_error( $customize_error_validation ) ) {
		foreach ( $customize_error_validation->get_error_messages() as $error ) {
			echo '<div id="error-centered"><strong>Error</strong>:';
			echo $error . '</div>';
		}
	}
}

/**
 * Registration user submission - inputs the user into the users table
 */
function boarding_registration_user_submission()
{
	global $customize_error_validation, $first_name, $last_name, $email, $username, $password, $council_number, $council_email;
	if ( 1 > count( $customize_error_validation->get_error_messages() ) ) {
		$userdata = array(
			'first_name'	=>   $first_name,
			'last_name' 	=>   $last_name,
			'user_email'	=>   $email,
			'user_login'	=>   $username,
			'user_pass' 	=>   $password,
			'role'			=>	'boarder'
		);

		$userid = wp_insert_user( $userdata );
		if( is_wp_error($userid) ) {
			echo $userid->get_error_message();
		} else {
			//add into custom table
			boarding_registration_data_submission($userid);
			send_council_email( $first_name, $last_name, $council_number, $council_email );
            send_customer_email( $email, $first_name, $last_name, $council_number );
		}

	} else {
		$customize_error_validation = new WP_Error;
		$customize_error_validation->add('field', 'There was an issue saving the username -  '. $username .' with the email '. $email .'');
	}
}

/**
 * @param $id
 * Registration data submission - inputs the users data into the home_boarding table via ID
 */
function boarding_registration_data_submission($id)
{
	global $wpdb, $first_name, $last_name, $town, $telephone, $boarding, $council_number, $council_email, $council_telephone, $council_file, $insurance_file, $gdpr;

	// process the file uploads
	$cfileUrl = council_file_upload( $council_file );
	$ifileUrl = insurance_file_upload( $insurance_file );

	$date = date("Y-m-d H:i:s");
	$userID = $id;
	$table_name = $wpdb->prefix . "home_boarding";

	$wpdb->insert( $table_name, array(
		'user_id' 			=> 	 $userID,
		'first_name'		=> 	 $first_name,
		'last_name'			=> 	 $last_name,
		'town'				=>   $town,
		'telephone' 		=>   $telephone,
		'boarding'			=>   $boarding,
		'council_number'    =>   $council_number,
		'council_email'		=>   $council_email,
		'council_telephone' =>   $council_telephone,
		'council_file'      =>   $cfileUrl,
		'insurance_file'    =>   $ifileUrl,
		'gdpr'              =>   $gdpr,
		'date_registered'   =>   $date,
		'verified'			=>	 'No'
	));
}

/**
 * Validate all of the submission form fields and sanitize entries before processing the data for submission
 */
function boarding_registration_form_function()
{
	global $first_name, $last_name, $town, $email, $username, $password, $telephone, $boarding, $council_number, $council_email, $council_telephone, $council_file, $insurance_file, $gdpr;

	if ( isset($_POST['submit'] ) ) {

		// check for valid details, again this needs extending to be more secure
		wp_reg_form_valid(
			$_POST['username'],
			$_POST['password'],
			$_POST['email'],
			$_POST['fname'],
			$_POST['lname']
		);

		// Sanitises the input fields and assigns them to the variable
		$username   		=   sanitize_user( $_POST['username'] );
		$password   		=   esc_attr( $_POST['password'] );
		$email  			=   sanitize_email( $_POST['email'] );
		$first_name 		=   sanitize_text_field( $_POST['fname'] );
		$last_name  		=   sanitize_text_field( $_POST['lname'] );
		$town				=   sanitize_text_field( $_POST['town'] );
		$telephone  		=   sanitize_text_field( $_POST['telephone'] );
		$boarding  			=   $_POST['boarding'];
		$gdpr               =   $_POST['gdpr'];
		$council_file       =   $_FILES['cfile'];
		$insurance_file     =   $_FILES['ifile'];
		$council_number     =   sanitize_text_field( $_POST['cnumber']);
		$council_email  	=   sanitize_email( $_POST['cemail'] );
		$council_telephone  =   sanitize_text_field( $_POST['ctelephone'] );

		boarding_registration_user_submission(
			$username,
			$password,
			$email,
			$first_name,
			$last_name,
			$town,
			$telephone,
			$boarding,
			$council_number,
			$council_email,
			$council_telephone,
			$council_file,
			$insurance_file,
			$gdpr
		);
	}

	boarding_registration_form(
		$username,
		$password,
		$email,
		$first_name,
		$last_name,
		$town,
		$telephone,
		$boarding,
		$council_number,
		$council_email,
		$council_telephone,
		$council_file,
		$insurance_file,
		$gdpr
	);
}

/**
 * @param $first_name
 * @param $last_name
 * @param $council_number
 * @param $council_email
 * Sends the council an email when someone has registered in order to validate the licence
 */
function send_council_email( $first_name, $last_name, $council_number, $council_email )
{
	$to = $council_email;
	$from = 'support@housemypet.com';
	$fromName = 'House my Pet';
	$subject = "Home Boarding Licence: $council_number.";

	// Get HTML contents from file
	$plugin_url = plugin_dir_url( dirname( __FILE__ ) );

	$htmlContent = file_get_contents(dirname( __DIR__ ) . '/mail/council-email.php');
	$htmlContent = str_replace("[fname]","$first_name", $htmlContent);
	$htmlContent = str_replace("[lname]","$last_name", $htmlContent);
	$htmlContent = str_replace("[cnumber]","$council_number", $htmlContent);
	$htmlContent = str_replace("[pluginurl]","$plugin_url", $htmlContent);

	// Set content-type header for sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// Additional headers
	$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n";
	$headers .= 'Cc: sheena@housemypet.com' . "\r\n";

	// Send email
	mail($to, $subject, $htmlContent, $headers);
}

/**
 * @param $email
 * @param $first_name
 * @param $last_name
 * @param $council_number
 * Sends an email to the customer when they have completed registration
 */
function send_customer_email( $email, $first_name, $last_name, $council_number )
{
	$to = $email;
	$from = 'support@housemypet.com';
	$fromName = 'House my Pet';
	$subject = "Thank you for Registering as a Home Boarder";

	// Get HTML contents from file
	$plugin_url = plugin_dir_url( dirname( __FILE__ ) );

	$htmlContent = file_get_contents(dirname( __DIR__ ) . "/mail/customer-email.php");
	$htmlContent = str_replace("[fname]","$first_name", $htmlContent);
	$htmlContent = str_replace("[lname]","$last_name", $htmlContent);
	$htmlContent = str_replace("[cnumber]","$council_number", $htmlContent);
	$htmlContent = str_replace("[pluginurl]","$plugin_url", $htmlContent);

	// Set content-type header for sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// Additional headers
	$headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n";
	$headers .= 'Cc: sheena@housemypet.com' . "\r\n";

	// Send email
	mail($to, $subject, $htmlContent, $headers);
}

/**
 * @param $council_file
 *
 * @return mixed
 * Handles the council licence file that is uploaded and stored in it's own directory
 */
function council_file_upload( $council_file )
{
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

	$fileValue = $council_file;

	if($fileValue['name'] != '')
	{
		// remove the initial directory for uploading files and use it's own
		add_filter('upload_dir', 'boarding_licence_directory');
		$fileUpload = wp_handle_upload($fileValue, array('test_form' => false));
		remove_filter('upload_dir', 'boarding_licence_directory');

		if ( $fileUpload && !isset( $fileUpload['error'] ) )
		{
			return $fileUpload['url'];
		} else {
			/**
			 * Error generated by _wp_handle_upload()
			 * @see _wp_handle_upload() in wp-admin/includes/file.php
			 */
			echo $fileUpload['error'];
		}
	}
}

/**
 * @param $dir
 *
 * @return array
 * Create the boarding licence directory for storing licence files
 */
function boarding_licence_directory($dir)
{
	return array (
		       'path'   => $dir['basedir'] . '/Boarding-Licences',
		       'url'    => $dir['baseurl'] . '/Boarding-Licences',
		       'subdir' => '/Boarding-Licences',
	       ) + $dir;

}

/**
 * @param $insurance_file
 *
 * @return mixed
 * Handles the insurance file that is uploaded and stored in it's own directory
 */
function insurance_file_upload( $insurance_file )
{
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

	$fileValue = $insurance_file;

	if($fileValue['name'] != '')
	{
		add_filter('upload_dir', 'boarding_insurance_directory');
		$fileUpload = wp_handle_upload($fileValue, array('test_form' => false));
		remove_filter('upload_dir', 'boarding_insurance_directory');

		if ( $fileUpload && !isset( $fileUpload['error'] ) )
		{
			return $fileUpload['url'];
		} else {
			/**
			 * Error generated by _wp_handle_upload()
			 * @see _wp_handle_upload() in wp-admin/includes/file.php
			 */
			echo $fileUpload['error'];
		}
	}
}

/**
 * @param $dir
 *
 * @return array
 * Create the boarding insurance directory for storing insurance files
 */
function boarding_insurance_directory($dir)
{
	return array (
		       'path'   => $dir['basedir'] . '/Insurance-Certificates',
		       'url'    => $dir['baseurl'] . '/Insurance-Certificates',
		       'subdir' => '/Insurance-Certificates',
	       ) + $dir;

}