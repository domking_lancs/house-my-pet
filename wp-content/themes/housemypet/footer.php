<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package House_my_Pet
 */

?>

<!-- footer mark up content here -->

<?php wp_footer(); ?>

</body>
</html>
